
// Melakukan proses penambahan data
	function addData_Proses()
	{
		var nama_add_proses = $('#add_nama').val();
		var doa_add_proses = $('#add_doa').val();

		var dbRef_add_proses = firebase.database();

		// Isikan data kedalam firebase
		var requestsBuku = dbRef_add_proses.ref("RequestsDoa/"+ nama_add_proses);

		requestsBuku.set( {
		
			nama : nama_add_proses,
			doa : doa_add_proses,
			
  		});

		// tampilData();
	}

//===============================================================================================================
// Melakukan proses penambahan data
function addData()
{
	var id_add_proses = $('#T4_add').val();
	var kategori_add_proses = $('#t4_kategori_add').val();
	var judul_add_proses = $('#t4_judul_add').val();
	var pengarang_add_proses = $('#t4_pengarang_add').val();
	var penerbit_add_proses = $('#t4_penerbit_add').val();
	var isbn_add_proses = $('#t4_isbn_add').val();
	var jumbuk_proses = $('#t4_jumbuk_add').val();

	var dbRef_add_proses = firebase.database();

	// Isikan data kedalam firebase
	var requestsBuku = dbRef_add_proses.ref("RequestsBuku/"+ id_add_proses);

	requestsBuku.set( {
	
		id : id_add_proses,
		kategori : kategori_add_proses,
		judul : judul_add_proses,
		pengarang : pengarang_add_proses,
		penerbit : penerbit_add_proses,
		isbn : isbn_add_proses,
		jumlah : jumbuk_proses
		
	  });

	$('#ModalAdd').modal('hide');

	tampilData();
}
